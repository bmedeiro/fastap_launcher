package com.example.hcilab.launcher2.Drag_Drop;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


import com.example.hcilab.launcher.threeButtons.R;
import com.example.hcilab.launcher2.Apps_Details_Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jbonk on 6/12/2017.
 */

public class SimpleBoardAdapter extends BoardAdapter {
    int header_resource;
    int item_resource;
    public SimpleBoardAdapter instance;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public SimpleBoardAdapter(Context context, ArrayList<SimpleColumn> data) {
        super(context);
        instance = this;
        this.columns = (ArrayList)data;
        this.header_resource = R.layout.column_header;
        this.item_resource = R.layout.column_item;

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public int getColumnCount() {
        return columns.size();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public int getItemCount(int column_position) {
        return columns.get(column_position).object.size();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public Object createHeaderObject(int column_position) {
        return columns.get(column_position).header_object;
    }

    @Override
    public Object createFooterObject(int column_position) {
        return null;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public Apps_Details_Data createItemObject(int column_position, int item_position) {
        return columns.get(column_position).object.get(item_position);
    }

    @Override
    public boolean isColumnLocked(int column_position) {
        return false;
    }

    @Override
    public boolean isItemLocked(int column_position) {
        return false;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public View createItemView(Context context,Object header_object,Object item_object,int column_position, int item_position) {
        View item = View.inflate(context, item_resource, null);
        ImageView icon = (ImageView) item.findViewById(com.example.drag_drop.R.id.icon);
        icon.setImageDrawable(columns.get(column_position).object.get(item_position).icon);
        TextView name = (TextView) item.findViewById(com.example.drag_drop.R.id.name);
        name.setText((columns.get(column_position).names.get(item_position).label));



        return item;
    }

    @Override
    public int maxItemCount(int column_position) {
        return 4;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void createColumns() {
        super.createColumns();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public View createHeaderView(Context context,Object header_object,int column_position) {
        View column = View.inflate(context, header_resource, null);
        TextView textView = (TextView)column.findViewById(com.example.drag_drop.R.id.textView);
        textView.setText(columns.get(column_position).header_object.toString());
        return column;
    }

    @Override
    public View createFooterView(Context context, Object footer_object, int column_position) {
           return null;
    }

    public static class SimpleColumn extends Column{
        public String title;
        public String name;
        public SimpleColumn(String title, List<Apps_Details_Data> items, List<Apps_Details_Data> names){
            super();
            this.title = title;
            this.header_object = new Item(title);
            this.names = names;
            this.object = items;
        }
    }

}
