package com.example.hcilab.launcher2;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import android.support.annotation.RequiresApi;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.example.hcilab.launcher.threeButtons.R;
import com.fxn.cue.Cue;
import com.fxn.cue.enums.Duration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import co.ceryle.fitgridview.FitGridView;

import static java.lang.String.valueOf;


public  class Fastap_Launcher_Main extends Activity{
    private PackageManager manager;
    Intent i;
    String array1[],array2[], array3[], array4[], array5[];
    private List<Integer> temp_list1, temp_list2, temp_list3, temp_list4, temp_list5;
    private List<Integer> tempIdForFindingPosition1, tempIdForFindingPosition2, tempIdForFindingPosition3, tempIdForFindingPosition4, tempIdForFindingPosition5;
    private List<Integer> tempIdForFindingIcon1, tempIdForFindingIcon2, tempIdForFindingIcon3, tempIdForFindingIcon4, tempIdForFindingIcon5;

    private FitGridView list;
    private List<Integer> cel;
    private List<Apps_Details_Data> apps;
    private List<Apps_Details_Data> firstListByUserPref, secondListByUserPref, thirdListByUserPref, fourthListByUserPref, fifthListByUserPref;
    private List<Apps_Details_Data> firstList,  secondList, thirdList, fourthList, fifthList, generalList;
    Apps_Details_Data selectedItem;
    int temp1 = -1;
    int temp2 = -1;
    int temp3 = -1;
    int temp4 = -1;
    int temp5 = -1;

    String path; // value used to indicate the path used to launch
    int pos, menu; // value used to catch touch values
    boolean vaiAbrir, vaiAbrir1, pararExpert, fecharNovice;
    int layout, drawable_A, drawable_B; // value used to set the layout address
    int pc; // value used to get the second touch
    int op; // value used to choose the layout
    Drawable icona;
    SharedPreferences prefs1, prefs2, prefs3, prefs4, prefs5;
    @SuppressLint("ClickableViewAccessibility")
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DisplayMetrics metrics = getApplicationContext().getResources().getDisplayMetrics();
        int height = metrics.heightPixels;
        int dpi = metrics.densityDpi;
        classifyLayout(height, dpi);

        setContentView(R.layout.grid);

        vaiAbrir = false;

        manager = getPackageManager();
        i = new Intent(Intent.ACTION_MAIN, null);
        i.addCategory(Intent.CATEGORY_LAUNCHER);
        prefs1 = getApplicationContext().getSharedPreferences("F1", 0);
        prefs2 = getApplicationContext().getSharedPreferences("F2", 0);
        prefs3 = getApplicationContext().getSharedPreferences("F3", 0);
        prefs4 = getApplicationContext().getSharedPreferences("F4", 0);
        prefs5 = getApplicationContext().getSharedPreferences("F5", 0);

        loadArray1ByUserPref("F1");
        loadArray2ByUserPref("F2");
        loadArray3ByUserPref("F3");
        loadArray4ByUserPref("F4");
        loadArray5ByUserPref("F5");


        loadApps1();
        loadApps2();
        loadApps3();
        loadApps4();
        loadApps5();
        unloadListView();


        list.setOnTouchListener(new View.OnTouchListener() {




            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @SuppressLint("ResourceAsColor")
            public boolean onTouch(View v, MotionEvent me) {
                vaiAbrir1=false;
                int index = me.getActionIndex();
                float x = me.getX(index);
                float y = me.getY(index);


                menu = list.pointToPosition((int) x, (int) y);
                pos = list.pointToPosition((int) x, (int) y);


                if (menu == 30) {
                    if(array1.length!=0) {
                        generalList = firstListByUserPref;

                    }else{
                        generalList =  firstList;
                    }
                    showapps(me, generalList, v);
                    vaiAbrir=true;

                }
                if (menu == 31) {

                    if(array2.length!=0) {
                        generalList = secondListByUserPref;

                    }else{
                        generalList = secondList;

                    }
                    showapps(me, generalList, v);

                    vaiAbrir=true;

                }
                if (menu == 32) {
                    if(array3.length!=0) {
                        generalList = thirdListByUserPref;

                    }else{
                        generalList = thirdList;

                    }
                    showapps(me, generalList, v);
                    vaiAbrir=true;

                }
                if (menu == 33) {
                    if(array4.length!=0) {
                        generalList = fourthListByUserPref;

                    }else{
                        generalList =  fourthList;
                    }
                    showapps(me, generalList, v);
                    vaiAbrir=true;


                }
                if (menu == 34) {
                    if(pc==29){ // verifying if the fifth column is open and the first icon (settings icon) is pressed
                        pc=-2; // in this way the code below will run, not the code in showapps()
                        Intent intent = new Intent(getApplicationContext(), listview.class);
                        startActivity(intent);
                        finish();
                        Log.d("pc", valueOf(pc));

                    }
                    generalList = fifthList;
                    showapps(me, generalList, v);
                    vaiAbrir = true;



                }


                fastapTechnique(pos, generalList, me);
                return false;
            }



        });


    }


    public String[] loadArray1ByUserPref(String arrayName) {
        int size = prefs1.getInt(arrayName + "_size", 0);
        array1 = new String[size];
        for(int i=0;i<size;i++) {
            array1[i] = prefs1.getString(arrayName + "_" + i, null);
            Log.d("Valores do array1", valueOf(array1[i]));

        }
        return array1;
    }
    public String[] loadArray2ByUserPref(String arrayName) {
        int size = prefs2.getInt(arrayName + "_size", 0);
        array2 = new String[size];
        for(int i=0;i<size;i++) {
            array2[i] = prefs2.getString(arrayName + "_" + i, null);

        }
        return array2;
    }
    public String[] loadArray3ByUserPref(String arrayName) {
        int size = prefs3.getInt(arrayName + "_size", 0);
        array3 = new String[size];
        for(int i=0;i<size;i++) {
            array3[i] = prefs3.getString(arrayName + "_" + i, null);

        }
        return array3;
    }
    public String[] loadArray4ByUserPref(String arrayName) {
        int size = prefs4.getInt(arrayName + "_size", 0);
        array4 = new String[size];
        for(int i=0;i<size;i++) {
            array4[i] = prefs4.getString(arrayName + "_" + i, null);

        }
        return array4;
    }
    public String[] loadArray5ByUserPref(String arrayName) {
        int size = prefs5.getInt(arrayName + "_size", 0);
        array5 = new String[size];
        for(int i=0;i<size;i++) {
            array5[i] = prefs5.getString(arrayName + "_" + i, null);

        }
        return array5;
    }

    private void ordering1ByUserPref() {
        temp_list1 = new ArrayList<Integer>();
        tempIdForFindingPosition1 = new ArrayList<Integer>();
        tempIdForFindingIcon1 = new ArrayList<Integer>();

        firstListByUserPref = new ArrayList<Apps_Details_Data>();
        for (int i = 0; i < array1.length; i++) {
            if (firstList.get(i).diretorio.contains(array1[i])) {
                firstListByUserPref.add(firstList.get(i));
                Log.d("intk", valueOf(i));

            } else {

                Log.d("aaaa", valueOf(firstList.get(i).label));
                Log.d("aaaa2e", valueOf(i));

                tempIdForFindingPosition1.add(i + 1);
                tempIdForFindingIcon1.add(i);

                for (int it = 0; it < array1.length; it++) {
                    if (firstList.get(it).diretorio.contains(array1[i])) {
                        temp1++;
                        temp_list1.add(it);

                        firstListByUserPref.add(firstList.get(it));


                    }


                }

            }
        }


        if (temp_list1 != null) {
            int templ = 0;
            for (int ik = 0; ik <= temp1; ik++) {

                Log.d("a1", valueOf(tempIdForFindingPosition1.get(temp1)));
                //Log.d("a2", valueOf(tempIdForFindingIcon.get(temp)));

                int tempRemove = temp_list1.get(ik) - templ++;
                if (tempRemove < 0) {
                    tempRemove = 0;
                }

                int position = tempIdForFindingPosition1.get(ik);

                firstListByUserPref.remove(tempRemove);
                firstListByUserPref.add(position, firstList.get(tempIdForFindingIcon1.get(ik)));


            }


        }

        if(firstListByUserPref.size()>30) {
            firstListByUserPref = firstListByUserPref.subList(0, 30);
        }

        if (firstListByUserPref.size() <= 29) {
            for (int k = firstListByUserPref.size(); k <= 29; k++) {
                Apps_Details_Data allAppss = new Apps_Details_Data();

                allAppss.label = null;
                allAppss.name = null;
                allAppss.icon = null;
                allAppss.diretorio = "zzzzzzzzzzzzzzzzzzzzzzzz";

                firstListByUserPref.add(allAppss);
            }

            Apps_Details_Data allAppss = new Apps_Details_Data();

            allAppss.label = null;
            allAppss.name = null;
            allAppss.setIcon(getResources().getDrawable(R.drawable.show));
            allAppss.diretorio = "zzzzzzzzzzzzzzzzzzzzzzzz";
            firstListByUserPref.add(allAppss);
            firstListByUserPref.add(allAppss);
            firstListByUserPref.add(allAppss);
            firstListByUserPref.add(allAppss);
            firstListByUserPref.add(allAppss);

        } else{
            Apps_Details_Data allAppss = new Apps_Details_Data();

            allAppss.label = null;
            allAppss.name = null;
            allAppss.setIcon(getResources().getDrawable(R.drawable.show));
            allAppss.diretorio = "zzzzzzzzzzzzzzzzzzzzzzzz";
            firstListByUserPref.add(allAppss);
            firstListByUserPref.add(allAppss);
            firstListByUserPref.add(allAppss);
            firstListByUserPref.add(allAppss);
            firstListByUserPref.add(allAppss);
        }
    }
    private void ordering2ByUserPref(){
        secondListByUserPref = new ArrayList<Apps_Details_Data>();
        temp_list2 = new ArrayList<Integer>();
        tempIdForFindingPosition2 = new ArrayList<Integer>();
        tempIdForFindingIcon2 = new ArrayList<Integer>();
        for(int i=0;i<array2.length;i++) {
            if (secondList.get(i).diretorio.contains(array2[i])) {
                secondListByUserPref.add(secondList.get(i));

            } else {
                tempIdForFindingPosition2.add(i + 1);
                tempIdForFindingIcon2.add(i);

                for (int it = 0; it < array2.length; it++) {
                    if (secondList.get(it).diretorio.contains(array2[i])) {
                        temp2++;
                        temp_list2.add(it);

                        secondListByUserPref.add(secondList.get(it));


                    }


                }

            }
        }


        if (temp_list2.size() != 0) {
            int templ = 0;
            for (int ik = 0; ik <= temp2; ik++) {
          /*  if(ik==3){
                ik =2;
            }*/
                int tempRemove = temp_list2.get(ik) - templ++;
                if (tempRemove < 0) {
                    tempRemove = 0;
                }

                int position = tempIdForFindingPosition2.get(ik);

                secondListByUserPref.remove(tempRemove);
                secondListByUserPref.add(position, secondList.get(tempIdForFindingIcon2.get(ik)));


            }


        }

        if(secondListByUserPref.size()>30) {
            secondListByUserPref = secondListByUserPref.subList(0, 30);
        }


        if(secondListByUserPref.size()<=29){
            for (int k = secondListByUserPref.size(); k <= 29; k++) {
                Apps_Details_Data allAppss = new Apps_Details_Data();

                allAppss.label = null;
                allAppss.name = null;
                allAppss.icon = null;
                allAppss.diretorio = "zzzzzzzzzzzzzzzzzzzzzzzz";

                secondListByUserPref.add(allAppss);
            }

            Apps_Details_Data allAppss = new Apps_Details_Data();

            allAppss.label = null;
            allAppss.name = null;
            allAppss.setIcon(getResources().getDrawable(R.drawable.show));
            allAppss.diretorio = "zzzzzzzzzzzzzzzzzzzzzzzz";

            secondListByUserPref.add(allAppss);
            secondListByUserPref.add(allAppss);
            secondListByUserPref.add(allAppss);
            secondListByUserPref.add(allAppss);
            secondListByUserPref.add(allAppss);
        } else{
            Apps_Details_Data allAppss = new Apps_Details_Data();

            allAppss.label = null;
            allAppss.name = null;
            allAppss.setIcon(getResources().getDrawable(R.drawable.show));
            allAppss.diretorio = "zzzzzzzzzzzzzzzzzzzzzzzz";

            secondListByUserPref.add(allAppss);
            secondListByUserPref.add(allAppss);
            secondListByUserPref.add(allAppss);
            secondListByUserPref.add(allAppss);
            secondListByUserPref.add(allAppss);
        }
    }
    private void ordering3ByUserPref(){
        thirdListByUserPref = new ArrayList<Apps_Details_Data>();
        temp_list3 = new ArrayList<Integer>();
        tempIdForFindingPosition3 = new ArrayList<Integer>();
        tempIdForFindingIcon3 = new ArrayList<Integer>();
        for(int i=0;i<array3.length;i++) {
            if (thirdList.get(i).diretorio.contains(array3[i])) {
                thirdListByUserPref.add(thirdList.get(i));

            } else {
                tempIdForFindingPosition3.add(i + 1);
                tempIdForFindingIcon3.add(i);

                for (int it = 0; it < array3.length; it++) {
                    if (thirdList.get(it).diretorio.contains(array3[i])) {
                        temp3++;
                        temp_list3.add(it);

                        thirdListByUserPref.add(thirdList.get(it));


                    }


                }

            }
        }


        if (temp_list3.size() != 0) {
            int templ = 0;
            for (int ik = 0; ik <= temp3; ik++) {

                int tempRemove = temp_list3.get(ik) - templ++;
                if (tempRemove < 0) {
                    tempRemove = 0;
                }
               int position = tempIdForFindingPosition3.get(ik);

                thirdListByUserPref.remove(tempRemove);
                thirdListByUserPref.add(position, thirdList.get(tempIdForFindingIcon3.get(ik)));


            }


        }

        if(thirdListByUserPref.size()>30) {

            thirdListByUserPref = thirdListByUserPref.subList(0, 30);
        }

        if(thirdListByUserPref.size()<=29){
            for (int k = thirdListByUserPref.size(); k <= 29; k++) {
                Apps_Details_Data allAppss = new Apps_Details_Data();

                allAppss.label = null;
                allAppss.name = null;
                allAppss.icon = null;
                allAppss.diretorio = "zzzzzzzzzzzzzzzzzzzzzzzz";

                thirdListByUserPref.add(allAppss);
            }

            Apps_Details_Data allAppss = new Apps_Details_Data();

            allAppss.label = null;
            allAppss.name = null;
            allAppss.setIcon(getResources().getDrawable(R.drawable.show));
            allAppss.diretorio = "zzzzzzzzzzzzzzzzzzzzzzzz";

            thirdListByUserPref.add(allAppss);
            thirdListByUserPref.add(allAppss);
            thirdListByUserPref.add(allAppss);
            thirdListByUserPref.add(allAppss);
            thirdListByUserPref.add(allAppss);
        } else{
            Apps_Details_Data allAppss = new Apps_Details_Data();

            allAppss.label = null;
            allAppss.name = null;
            allAppss.setIcon(getResources().getDrawable(R.drawable.show));
            allAppss.diretorio = "zzzzzzzzzzzzzzzzzzzzzzzz";

            thirdListByUserPref.add(allAppss);
            thirdListByUserPref.add(allAppss);
            thirdListByUserPref.add(allAppss);
            thirdListByUserPref.add(allAppss);
            thirdListByUserPref.add(allAppss);
        }
    }
    private void ordering4ByUserPref() {
        fourthListByUserPref = new ArrayList<Apps_Details_Data>();
        temp_list4 = new ArrayList<Integer>();
        tempIdForFindingPosition4 = new ArrayList<Integer>();
        tempIdForFindingIcon4 = new ArrayList<Integer>();
        for(int i=0;i<array4.length;i++) {
            if (fourthList.get(i).diretorio.contains(array4[i])) {
                fourthListByUserPref.add(fourthList.get(i));

            } else {
                tempIdForFindingPosition4.add(i + 1);
                tempIdForFindingIcon4.add(i);

                for (int it = 0; it < array4.length; it++) {
                    if (fourthList.get(it).diretorio.contains(array4[i])) {
                        temp4++;
                        temp_list4.add(it);

                        fourthListByUserPref.add(fourthList.get(it));


                    }


                }

            }
        }


        if (temp_list4.size() != 0) {
            int templ = 0;
            for (int ik = 0; ik <= temp4; ik++) {

                Log.d("a1", valueOf(tempIdForFindingPosition4.get(temp4)));
                //Log.d("a4", valueOf(tempIdForFindingIcon.get(temp)));

                int tempRemove = temp_list4.get(ik) - templ++;
                if (tempRemove < 0) {
                    tempRemove = 0;
                }

                int position = tempIdForFindingPosition4.get(ik);

                fourthListByUserPref.remove(tempRemove);
                fourthListByUserPref.add(position, fourthList.get(tempIdForFindingIcon4.get(ik)));


            }


        }
        if(fourthListByUserPref.size()>30) {
            fourthListByUserPref = fourthListByUserPref.subList(0, 30);
        }

        if(fourthListByUserPref.size()<=29){
            for (int k = fourthListByUserPref.size(); k <= 29; k++) {
                Apps_Details_Data allAppss = new Apps_Details_Data();

                allAppss.label = null;
                allAppss.name = null;
                allAppss.icon = null;
                allAppss.diretorio = "zzzzzzzzzzzzzzzzzzzzzzzz";

                fourthListByUserPref.add(allAppss);
            }

            Apps_Details_Data allAppss = new Apps_Details_Data();

            allAppss.label = null;
            allAppss.name = null;
            allAppss.setIcon(getResources().getDrawable(R.drawable.show));
            allAppss.diretorio = "zzzzzzzzzzzzzzzzzzzzzzzz";

            fourthListByUserPref.add(allAppss);
            fourthListByUserPref.add(allAppss);
            fourthListByUserPref.add(allAppss);
            fourthListByUserPref.add(allAppss);
            fourthListByUserPref.add(allAppss);
        } else{
            Apps_Details_Data allAppss = new Apps_Details_Data();

            allAppss.label = null;
            allAppss.name = null;
            allAppss.setIcon(getResources().getDrawable(R.drawable.show));
            allAppss.diretorio = "zzzzzzzzzzzzzzzzzzzzzzzz";

            fourthListByUserPref.add(allAppss);
            fourthListByUserPref.add(allAppss);
            fourthListByUserPref.add(allAppss);
            fourthListByUserPref.add(allAppss);
            fourthListByUserPref.add(allAppss);
        }
    }
    private void ordering5ByUserPref() {
        fifthListByUserPref = new ArrayList<Apps_Details_Data>();
        temp_list5 = new ArrayList<Integer>();
        tempIdForFindingPosition5 = new ArrayList<Integer>();
        tempIdForFindingIcon5 = new ArrayList<Integer>();
        for(int i=0;i<array5.length;i++) {
            if (fifthList.get(i).diretorio.contains(array5[i])) {
                fifthListByUserPref.add(fifthList.get(i));

            } else {
                tempIdForFindingPosition5.add(i + 1);
                tempIdForFindingIcon5.add(i);

                for (int it = 0; it < array5.length; it++) {
                    if (fifthList.get(it).diretorio.contains(array5[i])) {
                        temp5++;
                        temp_list5.add(it);

                        fifthListByUserPref.add(fifthList.get(it));


                    }


                }

            }
        }


        if (temp_list5.size() != 0) {
            int templ = 0;
            for (int ik = 0; ik <= temp5; ik++) {

                Log.d("a1", valueOf(tempIdForFindingPosition5.get(temp5)));
                //Log.d("a5", valueOf(tempIdForFindingIcon.get(temp)));

                int tempRemove = temp_list5.get(ik) - templ++;
                if (tempRemove < 0) {
                    tempRemove = 0;
                }

                int position = tempIdForFindingPosition5.get(ik);

                fifthListByUserPref.remove(tempRemove);
                fifthListByUserPref.add(position, fifthList.get(tempIdForFindingIcon5.get(ik)));


            }


        }
        if(fifthListByUserPref.size()>29) {
            fifthListByUserPref = fifthListByUserPref.subList(0, 29);
        }

        if(fifthListByUserPref.size()<=29){
            for (int k = fifthListByUserPref.size(); k < 29; k++) {
                Apps_Details_Data allAppss = new Apps_Details_Data();

                allAppss.label = null;
                allAppss.name = null;
                allAppss.icon = null;
                allAppss.diretorio = "zzzzzzzzzzzzzzzzzzzzzzzz";

                fifthListByUserPref.add(allAppss);
            }

            Apps_Details_Data allAppss = new Apps_Details_Data();

            allAppss.label = null;
            allAppss.name = null;
            allAppss.setIcon(getResources().getDrawable(R.drawable.show));
            allAppss.diretorio = "zzzzzzzzzzzzzzzzzzzzzzzz";

            fifthListByUserPref.add(allAppss);
            fifthListByUserPref.add(allAppss);
            fifthListByUserPref.add(allAppss);
            fifthListByUserPref.add(allAppss);
            fifthListByUserPref.add(allAppss);
        } else{
            Apps_Details_Data allAppss = new Apps_Details_Data();

            allAppss.label = null;
            allAppss.name = null;
            allAppss.setIcon(getResources().getDrawable(R.drawable.show));
            allAppss.diretorio = "zzzzzzzzzzzzzzzzzzzzzzzz";

            fifthListByUserPref.add(allAppss);
            fifthListByUserPref.add(allAppss);
            fifthListByUserPref.add(allAppss);
            fifthListByUserPref.add(allAppss);
            fifthListByUserPref.add(allAppss);
        }

    }
    private void classifyLayout(int height, int dpi){
        if (height==1280 && dpi == 320) {
            //Galaxy J7
            //4.7

            //56
            Log.d("k", valueOf(1));
            op=1;
            drawable_A = R.drawable.landmark1a;
            drawable_B = R.drawable.landmark1b;
            chooseLayout(op);
            Log.d("op", valueOf(1));

        }else if (height==1794) {
            //Nexus 5X

            //63
            Log.d("k", valueOf(2));
            op=2;
            drawable_A = R.drawable.landmark1a;
            drawable_B = R.drawable.landmark1b;
            chooseLayout(op);
            Log.d("op", valueOf(op));

        } else  if (height==1776) {
            //Pixel
            //Nexus 5

            //49.5
            Log.d("k", valueOf(3));
            op=3;
            drawable_A = R.drawable.landmark3a;
            drawable_B = R.drawable.landmark3b;
            chooseLayout(op);
            Log.d("op", valueOf(op));
        } else  if (height==2712) {
            //Pixel 2XL

            //75
            Log.d("k", valueOf(4));
            op=4;
            drawable_A = R.drawable.landmark1a;
            drawable_B = R.drawable.landmark1b;
            chooseLayout(op);
            Log.d("op", valueOf(op));
        } else  if (height==2392) {
            //Pixel XL
            //Nexus 6P
            //Nexus 6

            //63
            Log.d("k", valueOf(5));
            op=5;
            drawable_A = R.drawable.landmark1a;
            drawable_B = R.drawable.landmark1b;
            chooseLayout(op);
            Log.d("op", valueOf(op));
        } else  if (height==800&& dpi==240) {
            //Nexus One
            //Nexus S 
            //W480  //4
            //W480  //3.7

            // 41

            Log.d("k", valueOf(6));
            op=6;
            drawable_A = R.drawable.landmark13a;
            drawable_B = R.drawable.landmark13b;
            chooseLayout(op);
            Log.d("op", valueOf(op));

        }else  if (height== 1184) {
            //wd 768 //Nexus 4
            //wd 750 // Galaxy Nexus
            //w720  //4.65


            //49
            Log.d("k", valueOf(7));
            op=7;
            drawable_A = R.drawable.landmark3a;
            drawable_B = R.drawable.landmark3b;
            chooseLayout(op);
            Log.d("op", valueOf(op));
        } else   if (height== 854 && dpi==160) {
            //wd 480 //5.4
            //81
            Log.d("k", valueOf(8));
            op=8;
            drawable_A = R.drawable.landmark1a;
            drawable_B = R.drawable.landmark1b;
            chooseLayout(op);
            Log.d("op", valueOf(op));
        }else  if (height== 800 && dpi==160) {
            //wd 480 //5.1

            //81
            Log.d("k", valueOf(9));
            op=9;
            drawable_A = R.drawable.landmark1a;
            drawable_B = R.drawable.landmark1b;
            chooseLayout(op);
            Log.d("op", valueOf(op));
        }else if (height== 854 && dpi==240) {
            //wd 480 //3.7 slider

            //42
            Log.d("k", valueOf(10));
            op=10;
            drawable_A = R.drawable.landmark10a;
            drawable_B = R.drawable.landmark10b;
            chooseLayout(op);
            Log.d("op", valueOf(op));
        }else  if (height== 432  && dpi==120) {
            //wd 240 // 3.4

            //40
            Log.d("k", valueOf(11));
            op=11;
            drawable_A = R.drawable.landmark10a;
            drawable_B = R.drawable.landmark10b;
            chooseLayout(op);
            Log.d("op", valueOf(op));
        }else if (height==400 && dpi==120) {
            //wd 240 // 3.3

            //33
            Log.d("k", valueOf(12));
            op=12;
            drawable_A = R.drawable.landmark14a;
            drawable_B = R.drawable.landmark14b;
            chooseLayout(op);
            Log.d("op", valueOf(op));
        }else if (height==480 && dpi==160) {
            //wd 320 // 3.2
            //wd320  // 3.2 slider

            //29.5
            Log.d("k", valueOf(13));
            op=13;
            drawable_A = R.drawable.landmark13a;
            drawable_B = R.drawable.landmark13b;
            chooseLayout(op);
            Log.d("op", valueOf(op));
        } else if (height==320 && dpi==120) {
            //wd 240 // 2.7 slider
            //wd 240 // 2.7

            //29.5
            Log.d("k", valueOf(14));
            op=14;
            drawable_A = R.drawable.landmark14a;
            drawable_B = R.drawable.landmark14b;

            chooseLayout(op);
            Log.d("op", valueOf(op));
        } else{
            op=1;
            drawable_A = R.drawable.landmark1a;
            drawable_B = R.drawable.landmark1b;

            chooseLayout(op);
            Log.d("final", valueOf(op));

        }



    }
    private void chooseLayout(int op){
        if(op==1){ layout = R.layout.grid_item_720_1280; }
        if(op==2){ layout = R.layout.grid_item_1080_1794;}
        if(op==3){ layout = R.layout.grid_item_1080_1776; }
        if(op==4){ layout = R.layout.grid_item_1440_2712; }
        if(op==5){ layout = R.layout.grid_item_1440_2392; }
        if(op==6){ layout = R.layout.grid_item_480_800; }
        if(op==7){ layout = R.layout.grid_item_768_1184; }
        if(op==8){ layout = R.layout.grid_item_480_854; }
        if(op==9){ layout = R.layout.grid_item_5_1; }
        if(op==10){ layout = R.layout.grid_item_3_7_slider;}
        if(op==11){ layout = R.layout.grid_item_3_4; }
        if(op==12){ layout = R.layout.grid_item_3_3; }
        if(op==13){ layout = R.layout.grid_item_3_2; }
        if(op==14){ layout = R.layout.grid_item_2_7; }

    }
    @SuppressLint("ResourceAsColor")
    private void fastapTechnique(int pos, List<Apps_Details_Data> generalList, MotionEvent me) {

        cel = new ArrayList<>();

        if (pos != 30) {
            if (pos != -1) {
                cel.add(0, pos);

            }
        }


        if (cel.size() > 0) {
            Apps_Details_Data icon = (Apps_Details_Data) list.getItemAtPosition(cel.get(0));
            if (icon.name != null && vaiAbrir) {
                selectedItem = icon;
                selectedItem.position = cel.get(0);
                pc = selectedItem.position;
                path = selectedItem.name.toString();
                icona = selectedItem.getIcon();

                //(NAO ESQUEÇE, BRUNO. ISSO É PARA PARAR O FLASH)
                // if(pararExpert==false) {
                expertise(generalList);

                //  }

            }

        }



    }
    @SuppressLint("ClickableViewAccessibility")
    private void showapps(final MotionEvent me, final List<Apps_Details_Data> generalList, View view) {
        switch (me.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
                novice(generalList);

            case MotionEvent.ACTION_POINTER_DOWN:

                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP:
                if(pc==-2){
                    unloadListView();

                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (pc != -2 && generalList.get(pc).name != null) {
                            Intent iq = manager.getLaunchIntentForPackage(generalList.get(pc).name.toString());
                            Fastap_Launcher_Main.this.startActivity(iq);
                            unloadListView();

                        }

                        if(pc != -2 && generalList.get(pc).name == null){
                            unloadListView();
                        }
                        pc = -2;
                    }
                }, 550);

        }



    }
    private void unloadListView() {

        list = (FitGridView)  findViewById(R.id.apps_list);


        final ArrayAdapter<Apps_Details_Data> adapter = new ArrayAdapter<Apps_Details_Data>(this, layout, firstList) {
            @SuppressLint({"SetTextI18n", "ClickableViewAccessibility"})
            @Override
            public View getView(final int position, View convertView, ViewGroup parent) {

                if (convertView == null) {
                    convertView = getLayoutInflater().inflate(layout, null);
                }

                ImageView appIcon = (ImageView) convertView.findViewById(R.id.item_app_icon);

                if (position == 6 || position == 23) {
                    convertView.setBackgroundResource(drawable_A);
                } else if (position == 8 || position == 21) {
                    convertView.setBackgroundResource(drawable_B);
                }else
                    convertView.setBackgroundColor(getResources().getColor(R.color.colorBackground));

                if (position == 30 ||position == 31 || position == 32 || position == 33|| position == 34|| position == 35) {
                    appIcon.setImageDrawable(getResources().getDrawable(R.drawable.show));
                } else
                    appIcon.setImageDrawable(null);

                vaiAbrir=false;
                return convertView;
            }

        };
        hideAppLabel();
        list.setAdapter(adapter);
    }
    public void novice(final List<Apps_Details_Data> generalList) {
        pc = -2; // to not activate the first app on the list


        final ArrayAdapter<Apps_Details_Data> adapter = new ArrayAdapter<Apps_Details_Data>(this, layout, generalList) {
            @SuppressLint({"SetTextI18n", "ClickableViewAccessibility"})
            @Override
            public View getView(final int position, View convertView, ViewGroup parent) {

                if (convertView == null) {
                    convertView = getLayoutInflater().inflate(layout, null
                    );
                }

                ImageView appIcon = (ImageView) convertView.findViewById(R.id.item_app_icon);
                appIcon.setImageDrawable(generalList.get(position).getIcon());

                vaiAbrir = false;
                fecharNovice = true;


                if (position == 6 || position == 23) {
                    convertView.setBackgroundResource(drawable_A);
                } else if (position == 8 || position == 21) {
                    convertView.setBackgroundResource(drawable_B);
                }
                else
                    convertView.setBackgroundColor(getResources().getColor(R.color.colorBackground));

                return convertView;
            }


        };

        list.setAdapter(adapter);


    }
    public void expertise(final List<Apps_Details_Data> generalList) {


        list = (FitGridView)  findViewById(R.id.apps_list);

        final ArrayAdapter<Apps_Details_Data> adapter = new ArrayAdapter<Apps_Details_Data>(this, layout, generalList) {
            @SuppressLint({"SetTextI18n", "ClickableViewAccessibility", "ResourceAsColor"})
            @Override
            public View getView(final int position, View convertView, ViewGroup parent) {

                if (convertView == null) {
                    convertView = getLayoutInflater().inflate(layout, null
                    );
                }
                final ImageView appIcon = (ImageView) convertView.findViewById(R.id.item_app_icon);
                //    mostratudo=true;



                if (position == 30 || position == 31 || position == 32 || position == 33 || position == 34 || position == 35) {
                    appIcon.setImageDrawable(getResources().getDrawable(R.drawable.show));

                }

                for (int ib = pc; ib == position; ib++) {
                    appIcon.setImageDrawable(generalList.get(position).getIcon());
                    appIcon.setColorFilter(R.color.colorAccent);

                    AlphaAnimation animation1 = new AlphaAnimation(0.7f, 2f);
                    animation1.setDuration(20);
                    appIcon.setAlpha(1f);
                    appIcon.startAnimation(animation1);
                }


                vaiAbrir=false; // used to let the handler display all the icons

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        appIcon.setImageDrawable(generalList.get(position).getIcon());
                        appIcon.setColorFilter(null);
                        pararExpert = true;

                    }
                }, 600);



                if (position == 6 || position == 23) {
                    convertView.setBackgroundResource(drawable_A);
                } else if (position == 8 || position == 21) {
                    convertView.setBackgroundResource(drawable_B);
                }else
                    convertView.setBackgroundColor(getResources().getColor(R.color.colorBackground));
                return convertView;
            }
        };
        list.setAdapter(adapter);


    }
    private void showAppLabel(final List<Apps_Details_Data> generalList){
        list = (FitGridView)  findViewById(R.id.apps_list);

        list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> av, View v, final int pos, long id) {

                if(selectedItem!=null) {
                    toastAppLabel(generalList.get(pos).label.toString());
                }

                return false;
            }
        });
    }
    private void hideAppLabel(){
        list = (FitGridView)  findViewById(R.id.apps_list);

        list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> av, View v, final int pos, long id) {

                return false;
            }
        });
    }


    private void toastAppLabel(final String texto) {


        Cue.init()
                .with(this)
                .setGravity(Gravity.getAbsoluteGravity(pc+25,pc+10))
                .setMessage(texto)
                .setType(com.fxn.cue.enums.Type.PRIMARY)
                .setDuration(Duration.SHORT)
                .show();



    }

    private void loadApps1() {

        apps = new ArrayList<Apps_Details_Data>();



        List<ResolveInfo> availableActivities = manager.queryIntentActivities(i, 0);

        for (ResolveInfo ri : availableActivities) {

            Apps_Details_Data allApps = new Apps_Details_Data();

            allApps.label = ri.loadLabel(manager);
            allApps.name = ri.activityInfo.packageName;
            allApps.setIcon(ri.activityInfo.loadIcon(manager));
            allApps.diretorio = ri.loadLabel(manager).toString();
            apps.add(allApps);
            Collections.sort(apps);


        }
        Apps_Details_Data allApps = new Apps_Details_Data();
        if (apps.size() <= 154) {
            for (int k = apps.size(); k <= 154; k++) {
                allApps.label = null;
                allApps.name = null;
                allApps.setIcon(null);
                allApps.diretorio = "zzzzzzzzzzzzzzzzzzzzzzzz";

                apps.add(allApps);
                Collections.sort(apps);


            }
            Apps_Details_Data allAppss = new Apps_Details_Data();

            allAppss.label = null;
            allAppss.name = null;
            allAppss.setIcon(getResources().getDrawable(R.drawable.show));
            allAppss.diretorio = "zzzzzzzzzzzzzzzzzzzzzzzz";

            firstList = apps.subList(0, 30);
            firstList.add(allAppss);
            firstList.add(allAppss);
            firstList.add(allAppss);
            firstList.add(allAppss);
            firstList.add(allAppss);
            Collections.sort(firstList);
            if(array1 !=null) {
                ordering1ByUserPref();
            }
        }
    }
    private void loadApps2() {

        apps = new ArrayList<Apps_Details_Data>();



        List<ResolveInfo> availableActivities = manager.queryIntentActivities(i, 0);

        for (ResolveInfo ri : availableActivities) {

            Apps_Details_Data allApps = new Apps_Details_Data();

            allApps.label = ri.loadLabel(manager);
            allApps.name = ri.activityInfo.packageName;
            allApps.setIcon(ri.activityInfo.loadIcon(manager));
            allApps.diretorio = ri.loadLabel(manager).toString();
            apps.add(allApps);
            Collections.sort(apps);


        }
        Apps_Details_Data allApps = new Apps_Details_Data();
        if (apps.size() <= 154) {
            for (int k = apps.size(); k <= 154; k++) {
                allApps.label = null;
                allApps.name = null;
                allApps.setIcon(null);
                allApps.diretorio = "zzzzzzzzzzzzzzzzzzzzzzzz";

                apps.add(allApps);
                Collections.sort(apps);


            }
            Apps_Details_Data allAppss = new Apps_Details_Data();

            allAppss.label = null;
            allAppss.name = null;
            allAppss.setIcon(getResources().getDrawable(R.drawable.show));
            allAppss.diretorio = "zzzzzzzzzzzzzzzzzzzzzzzz";

            secondList = apps.subList(30, 60);
            secondList.add(allAppss);
            secondList.add(allAppss);
            secondList.add(allAppss);
            secondList.add(allAppss);
            secondList.add(allAppss);
            Collections.sort(secondList);
            if(array2!=null) {
                ordering2ByUserPref();
            }


        }
    }
    private void loadApps3() {

        apps = new ArrayList<Apps_Details_Data>();



        List<ResolveInfo> availableActivities = manager.queryIntentActivities(i, 0);

        for (ResolveInfo ri : availableActivities) {

            Apps_Details_Data allApps = new Apps_Details_Data();

            allApps.label = ri.loadLabel(manager);
            allApps.name = ri.activityInfo.packageName;
            allApps.setIcon(ri.activityInfo.loadIcon(manager));
            allApps.diretorio = ri.loadLabel(manager).toString();
            apps.add(allApps);
            Collections.sort(apps);


        }
        Apps_Details_Data allApps = new Apps_Details_Data();
        if (apps.size() <= 154) {
            for (int k = apps.size(); k <= 154; k++) {
                allApps.label = null;
                allApps.name = null;
                allApps.setIcon(null);
                allApps.diretorio = "zzzzzzzzzzzzzzzzzzzzzzzz";

                apps.add(allApps);
                Collections.sort(apps);


            }
            Apps_Details_Data allAppss = new Apps_Details_Data();

            allAppss.label = null;
            allAppss.name = null;
            allAppss.setIcon(getResources().getDrawable(R.drawable.show));
            allAppss.diretorio = "zzzzzzzzzzzzzzzzzzzzzzzz";

            thirdList = apps.subList(60, 90);
            thirdList.add(allAppss);
            thirdList.add(allAppss);
            thirdList.add(allAppss);
            thirdList.add(allAppss);
            thirdList.add(allAppss);
            Collections.sort(thirdList);
            if(array3!=null) {
                ordering3ByUserPref();
            }


        }
    }
    private void loadApps4() {

        apps = new ArrayList<Apps_Details_Data>();



        List<ResolveInfo> availableActivities = manager.queryIntentActivities(i, 0);

        for (ResolveInfo ri : availableActivities) {

            Apps_Details_Data allApps = new Apps_Details_Data();

            allApps.label = ri.loadLabel(manager);
            allApps.name = ri.activityInfo.packageName;
            allApps.setIcon(ri.activityInfo.loadIcon(manager));
            allApps.diretorio = ri.loadLabel(manager).toString();
            apps.add(allApps);
            Collections.sort(apps);


        }
        Apps_Details_Data allApps = new Apps_Details_Data();
        if (apps.size() <= 154) {
            for (int k = apps.size(); k <= 154; k++) {
                allApps.label = null;
                allApps.name = null;
                allApps.setIcon(null);
                allApps.diretorio = "zzzzzzzzzzzzzzzzzzzzzzzz";

                apps.add(allApps);
                Collections.sort(apps);


            }
            Apps_Details_Data allAppss = new Apps_Details_Data();

            allAppss.label = null;
            allAppss.name = null;
            allAppss.setIcon(getResources().getDrawable(R.drawable.show));
            allAppss.diretorio = "zzzzzzzzzzzzzzzzzzzzzzzz";

            fourthList = apps.subList(90, 120);
            fourthList.add(allAppss);
            fourthList.add(allAppss);
            fourthList.add(allAppss);
            fourthList.add(allAppss);
            fourthList.add(allAppss);
            Collections.sort(fourthList);

            if(array4!=null) {
                ordering4ByUserPref();
            }

        }
    }
    private void loadApps5() {

        apps = new ArrayList<Apps_Details_Data>();



        List<ResolveInfo> availableActivities = manager.queryIntentActivities(i, 0);

        for (ResolveInfo ri : availableActivities) {

            Apps_Details_Data allApps = new Apps_Details_Data();

            allApps.label = ri.loadLabel(manager);
            allApps.name = ri.activityInfo.packageName;
            allApps.setIcon(ri.activityInfo.loadIcon(manager));
            allApps.diretorio = ri.loadLabel(manager).toString();
            apps.add(allApps);
            Collections.sort(apps);


        }
        Apps_Details_Data allApps = new Apps_Details_Data();
        if (apps.size() <= 154) {
            for (int k = apps.size(); k <= 154; k++) {
                allApps.label = null;
                allApps.name = null;
                allApps.setIcon(null);
                allApps.diretorio = "zzzzzzzzzzzzzzzzzzzzzzzz";

                apps.add(allApps);
                Collections.sort(apps);


            }
            Apps_Details_Data allAppss = new Apps_Details_Data();

            allAppss.label = null;
            allAppss.name = null;
            allAppss.setIcon(getResources().getDrawable(R.drawable.show));
            allAppss.diretorio = "zzzzzzzzzzzzzzzzzzzzzzzz";

            Apps_Details_Data settings = new Apps_Details_Data();

            settings.label = "settings";
            settings.name = "settings";
            settings.setIcon(getResources().getDrawable(R.drawable.settings));
            settings.diretorio = "zzzzzzzzzzzzzzzzzzzzzzzz";

            fifthList = apps.subList(120, 149);

            fifthList.add(settings);
            fifthList.add(allAppss);
            fifthList.add(allAppss);
            fifthList.add(allAppss);
            fifthList.add(allAppss);
            fifthList.add(allAppss);
            Collections.sort(fifthList);


            if(array5!=null) {
                ordering5ByUserPref();
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, Fastap_Launcher_Main.class);
        startActivity(intent);
        finish();
    }


}
