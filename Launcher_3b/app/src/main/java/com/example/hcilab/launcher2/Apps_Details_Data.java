package com.example.hcilab.launcher2;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;

import com.example.hcilab.launcher.threeButtons.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class Apps_Details_Data implements Parcelable, Comparable {
    public  String diretorio;
    public  CharSequence label;
    public  CharSequence name;
    public  Drawable icon;
    public  int position;


    protected Apps_Details_Data(Parcel in) {
        diretorio = in.readString();
        position = in.readInt();
        label = in.readString();
        diretorio = in.readString();

    }

    public static final Creator<Apps_Details_Data> CREATOR = new Creator<Apps_Details_Data>() {
        @Override
        public Apps_Details_Data createFromParcel(Parcel in) {
            return new Apps_Details_Data(in);
        }

        @Override
        public Apps_Details_Data[] newArray(int size) {
            return new Apps_Details_Data[size];
        }
    };

    public Apps_Details_Data() {

    }


    public int compareTo(Object otherObject) {
        Apps_Details_Data outroApp = (Apps_Details_Data) otherObject;
        return this.diretorio.compareTo(outroApp.diretorio);

    }

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(diretorio);
        parcel.writeInt(position);
        parcel.writeString((String) label);
        parcel.writeString(diretorio);

    }

}
