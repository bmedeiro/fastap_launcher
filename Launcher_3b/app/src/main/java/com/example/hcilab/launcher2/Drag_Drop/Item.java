package com.example.hcilab.launcher2.Drag_Drop;

/**
 * Created by jbonk on 5/5/2018.
 */

public class Item{

    public String item;

    public Item(String item){
        this.item = item;
    }

    public String toString(){
        return item;
    }
}