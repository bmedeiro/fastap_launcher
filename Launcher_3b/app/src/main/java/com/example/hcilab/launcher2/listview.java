package com.example.hcilab.launcher2;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;


import com.example.hcilab.launcher.threeButtons.R;
import com.example.hcilab.launcher2.Drag_Drop.BoardView;
import com.example.hcilab.launcher2.Drag_Drop.SimpleBoardAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.example.hcilab.launcher.threeButtons.R.layout.activity_listview;
import static java.lang.String.valueOf;

public class listview extends AppCompatActivity  {
    int posicaoNova, posicaoAnterior, posicaoAtualizada , coluna;
    SimpleBoardAdapter boardAdapter;
    ArrayList<SimpleBoardAdapter.SimpleColumn> data;
    ArrayList<Apps_Details_Data> apps;
    List<Apps_Details_Data> firstList, secondList, thirdList, fourthList, fifthList;
    ArrayList<Object> obj1, obj2, obj3, obj4, obj5;

    BoardView boardView;
    SharedPreferences.Editor editor1, editor2, editor3, editor4, editor5;
    private PackageManager manager;
    Intent i;

    @TargetApi(Build.VERSION_CODES.M)
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(activity_listview);
        data = new ArrayList<>();
        firstList = new ArrayList<Apps_Details_Data>();
        secondList = new ArrayList<Apps_Details_Data>();
        thirdList = new ArrayList<Apps_Details_Data>();
        fourthList = new ArrayList<Apps_Details_Data>();
        fifthList = new ArrayList<Apps_Details_Data>();

        boardView = (BoardView) findViewById(R.id.boardView);

        boardView.SetColumnSnap(true);




        boardView.setOnDragItemListener(new BoardView.DragItemStartCallback() {


            @Override
            public void startDrag(View view, int startItemPos, int startColumnPos) {
                posicaoAnterior = -5;
                posicaoNova = -5;
                posicaoAtualizada = -5;

            }

            @Override
            public void changedPosition(View view, int startItemPos, int startColumnPos, int newItemPos, int newColumnPos) {
                posicaoAnterior = -5;
                posicaoNova = -5;
                posicaoAtualizada = -5;
                coluna = newColumnPos+1;
            }

            @Override
            public void dragging(View view, MotionEvent motionEvent) {
                posicaoAnterior = -5;
                posicaoNova = -5;
                posicaoAtualizada = -5;

            }

            @Override
            public void endDrag(View view, int i, int i1, int i2, int i3) {
                Log.d("coluna:", valueOf(i)); // coluna que está
                Log.d("posição 1:", valueOf(i1)); // posiçao que está
                Log.d("posição 2:", valueOf(i2)); // posição que foi
                Log.d("coluna que foi:", valueOf(i3)); // coluna que foi

                posicaoAnterior = i1;
                posicaoNova = i2;
                posicaoAtualizada = i1+1;


                if(coluna==1)
                {
                    firstList.set(posicaoNova, firstList.get(posicaoAnterior));


                }
                else if(coluna==2)
                { secondList.set(posicaoNova, secondList.get(posicaoAnterior)); }
                else if(coluna==3)
                { thirdList.set(posicaoNova, thirdList.get(posicaoAnterior)); }
                else if(coluna==4)
                { fourthList.set(posicaoNova, fourthList.get(posicaoAnterior)); }
                else if(coluna==5)
                { fifthList.set(posicaoNova, fifthList.get(posicaoAnterior)); }


            }


        });


        SharedPreferences pref1 = getApplicationContext().getSharedPreferences("F1", 0); // 0 - for private mode
        editor1 = pref1.edit();
        SharedPreferences pref2 = getApplicationContext().getSharedPreferences("F2", 0); // 0 - for private mode
        editor2 = pref2.edit();
        SharedPreferences pref3 = getApplicationContext().getSharedPreferences("F3", 0); // 0 - for private mode
        editor3 = pref3.edit();
        SharedPreferences pref4 = getApplicationContext().getSharedPreferences("F4", 0); // 0 - for private mode
        editor4 = pref4.edit();
        SharedPreferences pref5 = getApplicationContext().getSharedPreferences("F5", 0); // 0 - for private mode
        editor5 = pref5.edit();
        loadApps1();
        loadApps2();
        loadApps3();
        loadApps4();
        loadApps5();
        carregarLista();


        final Button save = (Button) findViewById(R.id.clica);
        final Button original = (Button) findViewById(R.id.original);

        original.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (coluna == 0) {
                    obj1 = new ArrayList<Object>();
                    obj2 = new ArrayList<Object>();
                    obj3 = new ArrayList<Object>();
                    obj4 = new ArrayList<Object>();
                    obj5 = new ArrayList<Object>();

                    Intent intent = new Intent(listview.this, Fastap_Launcher_Main.class);
                    for (int i = 0; i <= 29; i++) {
                        obj1.add(firstList.get(i).diretorio);
                        obj2.add(secondList.get(i).diretorio);
                        obj3.add(thirdList.get(i).diretorio);
                        obj4.add(fourthList.get(i).diretorio);
                        obj5.add(fifthList.get(i).diretorio);

                    }

                    saveArray1(obj1, "F1", getApplicationContext());
                    saveArray2(obj2, "F2", getApplicationContext());
                    saveArray3(obj3, "F3", getApplicationContext());
                    saveArray4(obj4, "F4", getApplicationContext());
                    saveArray5(obj5, "F5", getApplicationContext());

                    startActivity(intent);

                }
            }


        });

        save.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View arg0){
                if(coluna==0) {
                    obj1 = new ArrayList<Object>();
                    obj2 = new ArrayList<Object>();
                    obj3 = new ArrayList<Object>();
                    obj4 = new ArrayList<Object>();
                    obj5 = new ArrayList<Object>();

                    Intent intent = new Intent(listview.this, Fastap_Launcher_Main.class);
                    for (int i = 0; i <= 29; i++){
                        obj1.add(firstList.get(i).diretorio);
                        obj2.add(secondList.get(i).diretorio);
                        obj3.add(thirdList.get(i).diretorio);
                        obj4.add(fourthList.get(i).diretorio);
                        obj5.add(fifthList.get(i).diretorio);

                        Log.d("UM", valueOf(obj1.get(i)));
                    }

                    saveArray1(obj1, "F1", getApplicationContext());
                    saveArray2(obj2, "F2", getApplicationContext());
                    saveArray3(obj3, "F3", getApplicationContext());
                    saveArray4(obj4, "F4", getApplicationContext());
                    saveArray5(obj5, "F5", getApplicationContext());

                    startActivity(intent);

                }
                if(coluna==1) {
                    obj1 = new ArrayList<Object>();
                    Intent intent = new Intent(listview.this, Fastap_Launcher_Main.class);
                    for (int i = 0; i <= 29; i++){
                        obj1.add(firstList.get(i).diretorio);
                    }
                    saveArray1(obj1, "F1", getApplicationContext());
                    startActivity(intent);
                }
                else if(coluna==2) {
                    obj2 = new ArrayList<Object>();
                    Intent intent = new Intent(listview.this, Fastap_Launcher_Main.class);
                    for (int i = 0; i <= 29; i++){
                        obj2.add(secondList.get(i).diretorio);
                    }
                    saveArray2(obj2, "F2", getApplicationContext());

                    startActivity(intent);

                }
                else if(coluna==3) {
                    obj3 = new ArrayList<Object>();
                    Intent intent = new Intent(listview.this, Fastap_Launcher_Main.class);
                    for (int i = 0; i <= 29; i++){
                        obj3.add(thirdList.get(i).diretorio);
                    }
                    saveArray3(obj3, "F3", getApplicationContext());
                    startActivity(intent);
                }

                else if(coluna==4) {
                    obj4 = new ArrayList<Object>();
                    Intent intent = new Intent(listview.this, Fastap_Launcher_Main.class);
                    for (int i = 0; i <= 29; i++){
                        obj4.add(fourthList.get(i).diretorio);
                    }
                    saveArray4(obj4, "F4", getApplicationContext());
                    startActivity(intent);

                }
                else if(coluna==5) {
                    obj5 = new ArrayList<Object>();

                    fifthList.set(posicaoNova, firstList.get(posicaoAnterior));
                    Intent intent = new Intent(listview.this, Fastap_Launcher_Main.class);
                    for (int i = 0; i <= 29; i++){
                        obj5.add(fifthList.get(i).diretorio);
                    }
                    saveArray5(obj5, "F5", getApplicationContext());
                    startActivity(intent);

                }


            }
        });

    }

    public boolean saveArray1(List<Object> array, String arrayName, Context mContext) {
        editor1.putInt(arrayName +"_size", array.size());
        for(int i=0;i<array.size();i++)
            editor1.putString(arrayName + "_" + i, array.get(i).toString());
        return editor1.commit();
    }
    public boolean saveArray2(List<Object> array, String arrayName, Context mContext) {
        editor2.putInt(arrayName +"_size", array.size());
        for(int i=0;i<array.size();i++)
            editor2.putString(arrayName + "_" + i, array.get(i).toString());
        return editor2.commit();
    }
    public boolean saveArray3(List<Object> array, String arrayName, Context mContext) {
        editor3.putInt(arrayName +"_size", array.size());
        for(int i=0;i<array.size();i++)
            editor3.putString(arrayName + "_" + i, array.get(i).toString());
        return editor3.commit();
    }
    public boolean saveArray4(List<Object> array, String arrayName, Context mContext) {
        editor4.putInt(arrayName +"_size", array.size());
        for(int i=0;i<array.size();i++)
            editor4.putString(arrayName + "_" + i, array.get(i).toString());
        return editor4.commit();
    }
    public boolean saveArray5(List<Object> array, String arrayName, Context mContext) {
        editor5.putInt(arrayName +"_size", array.size());
        for(int i=0;i<array.size();i++)
            editor5.putString(arrayName + "_" + i, array.get(i).toString());
        return editor5.commit();
    }

    private void loadApps1() {
        apps = new ArrayList<Apps_Details_Data>();

        manager = getPackageManager();
        i = new Intent(Intent.ACTION_MAIN, null);
        i.addCategory(Intent.CATEGORY_LAUNCHER);

        List<ResolveInfo> availableActivities = manager.queryIntentActivities(i, 0);

        for (ResolveInfo ri : availableActivities) {

            Apps_Details_Data allApps = new Apps_Details_Data();

            allApps.label = ri.loadLabel(manager);
            allApps.name = ri.activityInfo.packageName;
            allApps.setIcon(ri.activityInfo.loadIcon(manager));
            allApps.diretorio = ri.loadLabel(manager).toString();
            apps.add(allApps);
            Collections.sort(apps);



        }
        Apps_Details_Data allApps = new Apps_Details_Data();
        if (apps.size() <= 154) {
            for (int k = apps.size(); k <= 154; k++) {
                allApps.label = null;
                allApps.name = null;
                allApps.setIcon(null);
                allApps.diretorio = "zzzzzzzzzzzzzzzzzzzzzzzz";

                apps.add(allApps);
                Collections.sort(apps);


            }
        }

        firstList = apps.subList(0, 30);
        Collections.sort(firstList);



    }
    private void loadApps2() {
        apps = new ArrayList<Apps_Details_Data>();

        manager = getPackageManager();
        i = new Intent(Intent.ACTION_MAIN, null);
        i.addCategory(Intent.CATEGORY_LAUNCHER);

        List<ResolveInfo> availableActivities = manager.queryIntentActivities(i, 0);

        for (ResolveInfo ri : availableActivities) {

            Apps_Details_Data allApps = new Apps_Details_Data();

            allApps.label = ri.loadLabel(manager);
            allApps.name = ri.activityInfo.packageName;
            allApps.setIcon(ri.activityInfo.loadIcon(manager));
            allApps.diretorio = ri.loadLabel(manager).toString();
            apps.add(allApps);
            Collections.sort(apps);



        }
        Apps_Details_Data allApps = new Apps_Details_Data();
        if (apps.size() <= 154) {
            for (int k = apps.size(); k <= 154; k++) {
                allApps.label = null;
                allApps.name = null;
                allApps.setIcon(null);
                allApps.diretorio = "zzzzzzzzzzzzzzzzzzzzzzzz";

                apps.add(allApps);
                Collections.sort(apps);


            }
        }

        secondList = apps.subList(30,60);
        Collections.sort(secondList);
    }
    private void loadApps3() {
        apps = new ArrayList<Apps_Details_Data>();

        manager = getPackageManager();
        i = new Intent(Intent.ACTION_MAIN, null);
        i.addCategory(Intent.CATEGORY_LAUNCHER);

        List<ResolveInfo> availableActivities = manager.queryIntentActivities(i, 0);

        for (ResolveInfo ri : availableActivities) {

            Apps_Details_Data allApps = new Apps_Details_Data();

            allApps.label = ri.loadLabel(manager);
            allApps.name = ri.activityInfo.packageName;
            allApps.setIcon(ri.activityInfo.loadIcon(manager));
            allApps.diretorio = ri.loadLabel(manager).toString();
            apps.add(allApps);
            Collections.sort(apps);



        }
        Apps_Details_Data allApps = new Apps_Details_Data();
        if (apps.size() <= 154) {
            for (int k = apps.size(); k <= 154; k++) {
                allApps.label = null;
                allApps.name = null;
                allApps.setIcon(null);
                allApps.diretorio = "zzzzzzzzzzzzzzzzzzzzzzzz";

                apps.add(allApps);
                Collections.sort(apps);


            }
        }

        thirdList = apps.subList(60,90);
        Collections.sort(thirdList);



    }
    private void loadApps4() {
        apps = new ArrayList<Apps_Details_Data>();

        manager = getPackageManager();
        i = new Intent(Intent.ACTION_MAIN, null);
        i.addCategory(Intent.CATEGORY_LAUNCHER);

        List<ResolveInfo> availableActivities = manager.queryIntentActivities(i, 0);

        for (ResolveInfo ri : availableActivities) {

            Apps_Details_Data allApps = new Apps_Details_Data();

            allApps.label = ri.loadLabel(manager);
            allApps.name = ri.activityInfo.packageName;
            allApps.setIcon(ri.activityInfo.loadIcon(manager));
            allApps.diretorio = ri.loadLabel(manager).toString();
            apps.add(allApps);
            Collections.sort(apps);



        }
        Apps_Details_Data allApps = new Apps_Details_Data();
        if (apps.size() <= 154) {
            for (int k = apps.size(); k <= 154; k++) {
                allApps.label = null;
                allApps.name = null;
                allApps.setIcon(null);
                allApps.diretorio = "zzzzzzzzzzzzzzzzzzzzzzzz";

                apps.add(allApps);
                Collections.sort(apps);


            }
        }

        fourthList = apps.subList(90,120);
        Collections.sort(fourthList);



    }
    private void loadApps5() {
        apps = new ArrayList<Apps_Details_Data>();

        manager = getPackageManager();
        i = new Intent(Intent.ACTION_MAIN, null);
        i.addCategory(Intent.CATEGORY_LAUNCHER);

        List<ResolveInfo> availableActivities = manager.queryIntentActivities(i, 0);

        for (ResolveInfo ri : availableActivities) {

            Apps_Details_Data allApps = new Apps_Details_Data();

            allApps.label = ri.loadLabel(manager);
            allApps.name = ri.activityInfo.packageName;
            allApps.setIcon(ri.activityInfo.loadIcon(manager));
            allApps.diretorio = ri.loadLabel(manager).toString();
            apps.add(allApps);
            Collections.sort(apps);



        }
        Apps_Details_Data allApps = new Apps_Details_Data();
        if (apps.size() <= 154) {
            for (int k = apps.size(); k <= 154; k++) {
                allApps.label = null;
                allApps.name = null;
                allApps.setIcon(null);
                allApps.diretorio = "zzzzzzzzzzzzzzzzzzzzzzzz";

                apps.add(allApps);
                Collections.sort(apps);


            }
        }

        fifthList = apps.subList(120,150);
        Collections.sort(fifthList);



    }
    @SuppressLint("ResourceAsColor")
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void carregarLista() {
        List<Apps_Details_Data> firstListEdited = (ArrayList<Apps_Details_Data>) getIntent().getSerializableExtra("lista1");

        //if(firstListEdited.size()!=0){
          //  data.add(new SimpleBoardAdapter.SimpleColumn("Column 1",  firstListEdited, firstListEdited));
        //}else{
            data.add(new SimpleBoardAdapter.SimpleColumn("Column 1",  firstList, firstList));

        //}

        data.add(new SimpleBoardAdapter.SimpleColumn("Column 2",  secondList, secondList));
        data.add(new SimpleBoardAdapter.SimpleColumn("Column 3",  thirdList, thirdList));
        data.add(new SimpleBoardAdapter.SimpleColumn("Column 4",  fourthList, fourthList));
        data.add(new SimpleBoardAdapter.SimpleColumn("Column 5",  fifthList, fifthList));


        boardAdapter = new SimpleBoardAdapter(this,data);

        boardView.setAdapter(boardAdapter);

    }


}
